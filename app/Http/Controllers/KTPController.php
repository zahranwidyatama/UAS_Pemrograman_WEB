<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Penduduk;

class KTPController extends Controller
{
    public function index(Request $request)
    {
    	$penduduk = Penduduk::all();
    	return view("pages.ktp.index", compact(["penduduk"]));
    }

    public function add(Request $request)
    {
    	return view("pages.ktp.add");
    }

    public function insert(Request $request)
    {
    	$input = $request->all();
    	$penduduk = new Penduduk;
    	$penduduk->nik = $input["nik"];
    	$penduduk->nama = $input["nama"];
    	$penduduk->jenis_kelamin = $input["jk"];
    	$penduduk->tempat_lahir = $input["tempat_lahir"];
    	$penduduk->tgl_lahir = $input["tgl_lahir"];
    	$penduduk->alamat = $input["alamat"];
    	$penduduk->rt_rw = $input["rtrw"];
    	$penduduk->kelurahan = $input["kelurahan"];
    	$penduduk->kecamatan = $input["kecamatan"];
    	$penduduk->agama = $input["agama"];
    	$penduduk->status_perkawinan = $input["status"];
    	$penduduk->pekerjaan = $input["pekerjaan"];
    	$penduduk->kewarganegaraan = $input["kewarganegaraan"];
    	$penduduk->berlaku_hingga = $input["berlaku"];
    	$penduduk->save();

    	return redirect("/");
    }

    Public function edit(Request $request, $id)
    {
    	$penduduk = Penduduk::find($id);

    	return view("pages.ktp.edit", compact(["penduduk"]));
    } 

    public function update(Request $request)
    {
    	$input = $request->all();
    	$penduduk = Penduduk::find($input["id"]);
    	$penduduk->nama = $input["nama"];
    	$penduduk->jenis_kelamin = $input["jk"];
    	$penduduk->tempat_lahir = $input["tempat_lahir"];
    	$penduduk->tgl_lahir = $input["tgl_lahir"];
    	$penduduk->alamat = $input["alamat"];
    	$penduduk->rt_rw = $input["rtrw"];
    	$penduduk->kelurahan = $input["kelurahan"];
    	$penduduk->kecamatan = $input["kecamatan"];
    	$penduduk->agama = $input["agama"];
    	$penduduk->status_perkawinan = $input["status"];
    	$penduduk->pekerjaan = $input["pekerjaan"];
    	$penduduk->kewarganegaraan = $input["kewarganegaraan"];
    	$penduduk->berlaku_hingga = $input["berlaku"];
    	$penduduk->save();

    	return redirect("ktp");
    }

    public function delete(Request $request, $id)
    {
    	$penduduk = Penduduk::find($id);
    	$penduduk->delete();

    	return redirect('ktp');
    }
}