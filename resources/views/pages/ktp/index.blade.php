@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <center>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Penduduk</div>

                <div class="panel-body">
                    <a href="{{ url('tambah') }}">Tambah Penduduk</a><br>
                    <table class="table table-stripped table-bordered">
                        <tr>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>TTL</th>
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Agama</th>
                            <th>Status</th>
                            <th>Pekerjaan</th>
                            <th>Kewarganegaraan</th>
                            <th>Masa Berlaku</th>
                            <th>Aksi</th>
                        </tr>
                        @foreach($penduduk as $p)
                        <tr>
                            <td>{{ $p->nik }}</td>
                            <td>{{ $p->nama }}</td>
                            <td>{{ $p->tempat_lahir}}, {{ $p->tgl_lahir }}</td>
                            <td>{{ $p->jenis_kelamin }}</td>
                            <td>{{ $p->alamat }}</td>
                            <td>{{ $p->agama}}</td>
                            <td>{{ $p->status_perkawinan }}</td>
                            <td>{{ $p->pekerjaan }}</td>
                            <td>{{ $p->kewarganegaraan }}</td>
                            <td>{{ $p->berlaku_hingga }}</td>
                            <td><a href="{{ url('edit') }}/{{ $p->id }}">Edit</a>&nbsp;<a href="{{ url('delete') }}/{{ $p->id }}">Hapus</a>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</center>
@endsection
