@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Data Penduduk</div>

                <div class="panel-body">
                    <form method="post" action="{{ url('update') }}">
                        {!!csrf_field() !!}
                        <input type='hidden' name='id' value="{{ $penduduk->id }}">
                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" name="nik" class="form-control" value="{{ $penduduk->nik }}">
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class="form-control" value="{{ $penduduk->nama }}">
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input type="text" name="jk" class="form-control" value="{{ $penduduk->jenis_kelamin }}">
                        </div>
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" value="{{ $penduduk->tempat_lahir }}">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" name="tgl_lahir" class="form-control" value="{{ $penduduk->tgl_lahir }}">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" value="{{ $penduduk->alamat }}">
                        </div>
                        <div class="form-group">
                            <label>RT / RW</label>
                            <input type="text" name="rtrw" class="form-control" value="{{ $penduduk->rt_rw }}">
                        </div>
                        <div class="form-group">
                            <label>Kelurahan</label>
                            <input type="text" name="kelurahan" class="form-control" value="{{ $penduduk->kelurahan }}">
                        </div>
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input type="text" name="kecamatan" class="form-control" value="{{ $penduduk->kecamatan }}">
                        </div>
                        <div class="form-group">
                            <label>Agama</label>
                            <input type="text" name="agama" class="form-control" value="{{ $penduduk->agama }}">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status">
                                <option value="kawin">Kawin</option>
                                <option value="bkawin">Belum Kawin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pekerjaan</label>
                            <input type="text" name="pekerjaan" class="form-control" value="{{ $penduduk->pekerjaan }}">
                        </div>
                        <div class="form-group">
                            <label>Kewarganegaraan</label>
                            <input type="text" name="kewarganegaraan" class="form-control" value="{{ $penduduk->kewarganegaraan }}">
                        </div>
                        <div class="form-group">
                            <label>Berlaku Hingga</label>
                            <input type="text" name="berlaku" class="form-control" value="{{ $penduduk->berlaku_hingga }}">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="" class="btm btn-primary">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
