<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get("/home", 'HomeController@index')->name('home');
Route::get("/", 'KTPController@index');
Route::get("/tambah", 'KTPController@add');
Route::get("/edit/{id}", 'KTPController@edit');
Route::get("/delete/{id}", 'KTPController@delete');

Route::post("/insert", 'KTPController@insert');
Route::post("/update", 'KTPController@update');